'use strict'

// *****************************************************
// ********************** Clases ***********************
// *****************************************************

class Animal {
    constructor(name, legs, sound) {
        this.name = name;
        this.legs = legs;
        this.sound = sound;
        this.color = 'red';
    }

    // Dentro de una clase no es necesario poner function antes de una función
    animalSays() {
        return `Hola, soy ${this.name}, tengo ${this.legs} patas y digo ${this.sound}.`;
    }
}

// Instancia de la clase – siempre se declara una variable X = new (nombre de la clase)(propiedades)
const myAnimal = new Animal('Paco', 4, 'woof');

console.log(myAnimal.animalSays());
// Solución en consola: "Hola, soy Paco, tengo 4 patas y digo woof."

// Clase derivada de otra – hereda todas las propiedades y funciones de la clase padre.
class Cat extends Animal {
    constructor(name, legs, sound, whisker) {
        // Para heredar las propiedades de la clase padre, se utiliza "super(propiedades)"
        super(name, legs, sound);
        this.whisker = whisker;
    }
}

const newCat = new Cat('Petite', 4, 'meoooow', true);

console.log(newCat.animalSays());
// Solución en consola: "Hola, soy Petite, tengo 4 patas y digo meoooow."

// *****************************************************
// ***************** Ejercicio: Banco ******************
// *****************************************************

class Bank {
    constructor(name, address) {
        this.name = name;
        this.address = address;
        this.clients = [];
    }
}

class Client {
    constructor(name, accountID) {
        this.name = name;
        this.accountID = accountID;
    }
}

class BankAccount {
    constructor(id) {
        this.balance = 0;
        this.id = id;
    }
}

class Titular {
    constructor(name, genre, wallet) {
        this.name = name;
        this.genre = genre;
        this.wallet = wallet;
        this.id = this.generateID();
    }

    generateID() {
        Math.floor(Math.random() * 999999999);
    }

    openBankAccount(bank) {

        // Generamos un ID para la nueva cuenta bancaria
        const accountID = this.generateID();

        // Accedemos al array "clients" del banco y almacenamos el nombre y el ID de la cuenta bancaria del nuevo cliente
        bank.clients.push({
            name: this.name,
            clientID: accountID
        });
        // Creamos una nueva cuenta bancaria a la que debemos pasar el nuevo ID
        return new BankAccount(accountID);
    }

    depositMoney(amount, account) {

        // Comprobamos si el cliente tiene en la wallet la cantidad que quiere ingresar. Si no es así, mostramos un mensaje que diga que no tenemos suficiente dinero
        if (this.wallet > amount) {
            // Si tiene suficiente dinero, restamos en la wallet la cantidad que vamos a ingresar
            this.wallet -= amount;
            // Accedemos a la propiedad "balance" de la cuenta bancaria y sumamos la cantidad a ingresar. Mostramos un mensaje de que el ingreso ha sido realizado
            account.balance += amount;
            return `Se han ingresado ${amount}€`;
        }
        return `Not enough money :(`;
    }

    extractMoney(amount, account) {

        // Comprobamos si en la wallet tenemos la cantidad a extraer. Si no es así, mostramos un mensaje que diga que no tiene suficiente dinero
        if (account.balance > amount) {
            // Si tiene suficiente dinero, restamos la cantidad a extraer en el balance.
            account.balance -= amount;
            // Accedemos a la propiedad "wallet" del titular y sumamos a la wallet la cantidad ingresada. Mostramos un mensaje de que la extracción ha sido realizada.
            this.wallet += amount;
            return `Se han extraído ${amount}€`;
        }
        return `No tienes suficiente dinero en tu cuenta :(`;
    }

    showMoney(account) {

        // Accedemos a la propiedad "balance" de la cuenta y mostramos un mensaje que indique el saldo actual.
        return `Tienes ${account.balance}€ en tu cuenta.`;
    }
}

const abanca = new Bank('Abanca', 'Avenida de Oza, 18');

const misha = new Titular('Misha', 'man', 1500);

const mishasAccount = misha.openBankAccount(abanca);

misha.depositMoney(1000, mishasAccount);
misha.extractMoney(540, mishasAccount);

console.log(mishasAccount);