'use strict'

// ***********************************************************************
// ************ Ejercicio 1 : Best time to buy and sell stock ************
// ***********************************************************************

const prices = [7, 1, 5, 3, 6, 4];

// Creamos una función para calcular el beneficio
function maxProfit(prices) {

  // Establecemos una variable en la que guardamos el beneficio (mayor) que se obtiene
  let mayorLucro = 0;

  // Hacemos un bucle for para recorrer el array de precios
  for (let i = 0; i < prices.length - 1; i++) {
    // Hacemos otro bucle for para recorrer el array en la siguiente posición
    for (let j = i + 1; j < prices.length; j++) {
      // Establecemos una variable para guardar el valor del precio en la segunda posición menos el precio en la primera posición
      const lucro = prices[j] - prices[i];
      // Almacenamos el valor más alto entre "mayorLucro" y "lucro"
      mayorLucro = Math.max(mayorLucro, lucro);
    }
  }
  // Devolvemos la variable "mayorLucro", que nos da el mayor beneficio
  return mayorLucro;
}

console.log(maxProfit(prices));

// ***********************************************************************
// ***************** Ejercicio 2: Robot return to origin *****************
// ***********************************************************************

console.log('* * * * * * * * *');


const movements = "UD";

// Creamos las variables para cada posición
let up = 0;
let down = 0;
let left = 0;
let right = 0;

// Creamos una función para realizar los movimientos del robot
function robotMovements(movements) {

  // Establecemos una variable en la que dividimos el string de "movements".
  const splitMovements = movements.split('');

  // Hacemos un "for of" para recorrer el string 
  for (const move of splitMovements) {
    // Hacemos un switch y establecemos lo que debe hacer en los distintos casos
    switch (move) {
      // En el caso "U" se mueve 1 arriba
      case 'U': up++;
        break;

      // En el caso "D" se mueve 1 abajo
      case 'D': down++;
        break;

      // En el caso "U" se mueve 1 a la derecha
      case 'R': right++;
        break;

      // En el caso "default" se mueve 1 a la izquierda
      // default es "left" porque es la única opción que queda.
      default: left++;
    }
  }

  // Establecemos una variable en la que guardamos el valor de "right" menos "left"
  const differenceHorizontalMoves = right - left;
  // Establecemos una variable en la que guardamos el valor de "up" menos "down"
  const differenceVerticalMoves = up - down;

  // Creamos una condición para cuando tanto la resta de la variable "differenceHorizontalMoves" como la de "differenceVerticalMoves" dan resultado 0
  if (differenceHorizontalMoves === 0 && differenceVerticalMoves === 0) {
    // Si se cumple la condición, devuelve "true"
    return true;
  }
  // Si no se cumple la condición, devuelve "false"
  return false;
}

console.log(robotMovements(movements));

// **********************************************************************
// ***************** Ejercicio 3: Mover robot por array *****************
// **********************************************************************
console.log('* * * * * * * * *');

// Creamos la clase Robot
class Robot {
  constructor(space) {
    this.space = space;
    // Añadimos una propiedad "columns" que se va a mover por el array a la derecha y a la izquierda
    this.columns = 0;
    // Añadimos una propiedad "rows" que se va a mover por el array arriba y abajo
    this.rows = 0;
  }
  // Creamos un método para moverse hacia la izquierda
  moveLeft() {
    // Creamos una condición para que cuando estemos en la posición 0 (primera posición del array) y le digamos que se mueva hacia la izquierda, no se mueva más porque se saldría del array
    if (this.columns > 0) {
      this.columns--;
    }
  }
  // Creamos un método para moverse hacia la derecha
  moveRight() {
    // Creamos una condición para que cuando estemos en la última posición del array y le digamos que se mueva hacia la derecha, no se mueva más porque se saldría del array
    if (this.columns < this.space.length - 1) {
      this.columns++;
    }
  }
  // Creamos un método para moverse hacia arriba
  moveUp() {
    // Creamos una condición para que cuando estemos en la posición 0 (primera posición del array) y le digamos que se mueva hacia arriba, no se mueva más porque se saldría del array
    if (this.rows > 0) {
      this.rows--;
    }
  }
  // Creamos un método para moverse hacia abajo
  moveDown() {
    // Creamos una condición para que cuando estemos en la última posición del array (en la esquina inferior derecha) y le digamos que se mueva hacia abajo, no se mueva más porque se saldría del array
    if (this.rows < this.space.length - 1) {
      this.rows++;
    }
  }
  // Creamos un método que nos indique en que posición del array se encuentra
  currentPosition() {
    // Le decimos que nos devuelva la posición en el array en la que se encuentra
    return this.space[this.rows][this.columns];
  }
}


//
console.log('Ejemplo 1: derecha - izquierda');
//

const mySpace = [1, 9, 5, 8, 7, 6];

const myRobot = new Robot(mySpace);

console.log(myRobot.currentPosition()); // Salida: 1

myRobot.moveRight();
console.log(myRobot.currentPosition()); // Salida: 9

myRobot.moveLeft();
console.log(myRobot.currentPosition()); // Salida: 1

myRobot.moveLeft();
console.log(myRobot.currentPosition()); // Salida: 1

myRobot.moveRight();
myRobot.moveRight();
myRobot.moveRight();
console.log(myRobot.currentPosition()); // Salida: 8

//
console.log('Ejemplo 2: derecha - izquierda - arriba - abajo');
//

const mySpace2 = [
  [1, 9, 5],
  [7, 6, 3],
  [6, 6, 8]
]

const myRobot2 = new Robot(mySpace2);

console.log(myRobot2.getPosition()); // Salida: 1 [1, 9, 5]

myRobot2.moveRight();
myRobot2.moveRight();
myRobot2.moveRight();
console.log(myRobot2.getPosition()); // Salida: 5 [6, 6, 8]

myRobot2.moveDown();
myRobot2.moveDown();
console.log(myRobot2.getPosition()); // Salida: 8 [6, 6, 8]

myRobot2.moveUp();
console.log(myRobot2.getPosition()); // Salida: 3 [6, 6, 8]