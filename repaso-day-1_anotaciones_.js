'use strict';

// ***********************************************************************
// ********** Ejercicio 1: ¿Se repite algún número en el array? **********
// ***********************************************************************

const nums = [1, 2, 3, 1];

function isRepeat(arrayNums) {
    // Recorremos el array en cada posición
    for (let i = 0; i < nums.length; i++) {
        // Recorremos el array en la posición siguiente a la línea anterior para comparar el valor en ambas posiciones
        for (let j = i + 1; j < nums.length; j++) {
            // Creamos una condición que diga que si el valor en ambas posiciones es el mismo, devuelva "true"
            if (nums[i] === nums[j]) {
                return true;
            }
        }
    }
    // Si no se cumple la condición, devuelve "false"
    return false;
}

console.log(isRepeat(nums));

// ***********************************************************************
// *********** Ejercicio 2: Máxima cantidad de 1s consecutivos ***********
// ***********************************************************************

const setOfNumbers = [1, 1, 0, 1, 1, 1];

function findMaxConsecutiveOnes(setOfNumbers) {
    let consecutiveOnes = 0;
    let mayor = 0;

    // Recorremos el array en cada posición
    for (let i = 0; i < setOfNumbers.length; i++) {
        // Creamos una condición para cuando el valor en esa posición del array es igual a 1
        if (setOfNumbers[i] === 1) {
            // Si "i" es igual a 1, se recorre el array en la siguiente posición.
            consecutiveOnes++;
            // Se almacenan los datos cuándo hay un 1.
            // Creamos otra condición para cuando el valor de "consecutiveOnes" es mayor que el valor de "mayor"
            if (consecutiveOnes > mayor) {
                // Si se cumple la condición, decimos que "mayor" es igual a "consecutiveOnes"
                mayor = consecutiveOnes;
            }
        }
        // Creamos otra condición para cuando el valor en esa posición del array es igual a 0
        if (setOfNumbers[i] === 0) {
            // Si se cumple la condición le decimos que el valor de "consecutiveOnes" es igual a 0
            consecutiveOnes = 0;
        }
    }
    // Devolvemos el valor de "mayor"
    return mayor;
}

console.log(findMaxConsecutiveOnes(setOfNumbers));

// ***********************************************************************
// **** Ejercicio 3: Mover todos los 0s al final sin alterar el orden ****
// ***********************************************************************

const arrayOfNumbers = [0, 1, 0, 3, 12];

function moveZeroes(arrayOfNumbers) {
    // Recorremos el array en cada posición
    for (let i = 0; i < arrayOfNumbers.length; i++) {
        // Creamos una condición para indicar que debe hacer la función cuando hay un cero
        if (arrayOfNumbers[i] === 0) {
            // Guardamos los 0 en una variable
            let zeroes = arrayOfNumbers[i];
            // Eliminamos el 0 del array
            arrayOfNumbers.splice(i, 1);
            // Añadimos al final del array el cero que guardamos en la variable "zeroes"
            arrayOfNumbers.push(zeroes);
        }
    }
    // Devolvemos el array final
    return arrayOfNumbers;
}

console.log(moveZeroes(arrayOfNumbers));
