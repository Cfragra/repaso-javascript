'use strict';

// ***********************************************************************
// ********** Ejercicio 1: ¿Se repite algún número en el array? **********
// ***********************************************************************

const nums = [1, 2, 3, 1];

function isRepeat(arrayNums) {
  for (let i = 0; i < nums.length; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] === nums[j]) {
        return true;
      }
    }
  }
  return false;
}

console.log(isRepeat(nums));

// ***********************************************************************
// *********** Ejercicio 2: Máxima cantidad de 1s consecutivos ***********
// ***********************************************************************

const setOfNumbers = [1, 1, 0, 1, 1, 1];

function findMaxConsecutiveOnes(setOfNumbers) {
  let consecutiveOnes = 0;
  let mayor = 0;

  for (let i = 0; i < setOfNumbers.length; i++) {
    if (setOfNumbers[i] === 1) {
      // Si "i" es igual a 1, se recorre el array en la siguiente posición.
      consecutiveOnes++;
      // Se almacenan los datos cuaándo hay un 1.
      if (consecutiveOnes > mayor) {
        mayor = consecutiveOnes;
      }
    }
    if (setOfNumbers[i] === 0) {
      consecutiveOnes = 0;
    }
  }
  return mayor;
}

console.log(findMaxConsecutiveOnes(setOfNumbers));

// ***********************************************************************
// **** Ejercicio 3: Mover todos los 0s al final sin alterar el orden ****
// ***********************************************************************

const arrayOfNumbers = [0, 1, 0, 3, 12];

function moveZeroes(arrayOfNumbers) {
  for (let i = 0; i < arrayOfNumbers.length; i++) {
    if (arrayOfNumbers[i] === 0) {
      let zeroes = arrayOfNumbers[i];
      arrayOfNumbers.splice(i, 1);
      arrayOfNumbers.push(zeroes);
    }
  }
  return arrayOfNumbers;
}

console.log(moveZeroes(arrayOfNumbers));
