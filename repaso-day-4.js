// Asynchronous

const first = () => {
    console.log('Hello! Soy la primera función.');
    second();
    console.log('Se ha acabado.')
};

const second = () => {
    setTimeout(() => {
        console.log('¡Yo soy la segunda función!');
    }, 2000)
};

first();

// ******************************************************
// ********************* EJEMPLO 1  *********************
// ******************************************************

const recetasID = [10, 20, 30, 40, 50]

const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        // Creamos un array de IDs
        resolve(recetasID);
        reject('Error');
    }, 1500);
});

const getRecipe = recipeID => {
    return new Promise((resolve, reject) => {
        setTimeout(
            ID => {
                // Creamos un objeto simulando una receta
                const recipe = {
                    title: 'Macarrones a la putanesca',
                    publisher: 'Pablo González',
                };
                resolve(`${ID}: ${recipe.title}`);
            }, 1500, recipeID
        );
    });
};

// Con 'then / catch" manejamos los datos almacenados en la promesa creada anteriormente.

getIDs.then(arrayRecipesID => {
    console.log(arrayRecipesID);
    return getRecipe(arrayRecipesID[2]);
})
    .then(recipe => {
        console.log(recipe);
    })
    .catch(error => {
        console.log(error);
    });

// ******************************************************
// ********************* EJEMPLO  2 *********************
// ******************************************************

const recipesBD = [
    {
        publisher: 'Pablo',
        recipes: [
            { title: 'Pizza margarita' },
            { title: 'Macarrones a la putanesca' }
        ]
    },
    {
        publisher: 'Sara',
        recipes: [
            { title: 'Ensalada' },
            { title: 'Ensalada de pasta' }
        ]
    },
    {
        publisher: 'Lucía',
        recipes: [
            { title: 'Hamburguesa vegana' },
            { title: 'Dorada al horno' }
        ]
    },
]

const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(recipesBD);
    }, 1500);
});

const getRecipe = publisher => {
    return new Promise((resolve, reject) => {
        setTimeout(
            publish => {
                resolve(publish.recipes[0])
            }, 1500, publisher);
    });
};

// "await" solo puede ser usado en una función "async"
async function getRecipesAW() {
    const recipesIDs = await getIDs;
    console.log(recipesIDs);
    const recipe = await getRecipe(recipesIDs[0]);
    console.log(recipe);

    return recipe;
}

// const rec = getRecipesAW();
// console.log(rec);

getRecipesAW()//.then(data => console.log(data));

// ******************************************************
// ******************** EJERCICIO  1 ********************
// ******************************************************

/*
En un aula encontramos un número indeterminado de pupitres organizados en dos columnas. Algunos pupitres están ocupados mientras otros están vacíos.

El array "ocuppiedDesks" sigue el siguiente formato: `[K, r1, r2, r2, ...]` donde `K` es el total de pupitres que hay en el aula. El resto de valores se corresponden a los pupitres que ya están ocupados.

Debes averiguar la cantidad de formas en que 2 estudiantes pueden sentarse uno al lado del otro. Esto significa que 1 estudiante está a la izquierda y 1 estudiante a la derecha, o que 1 estudiante está directamente arriba o abajo de otro estudiante.

Ejemplo: `[12, 2 ,6, 7, 11]`

Basándose en esta disposición de escritorios ocupados, hay un total de 6 formas de acomodar a 2 nuevos estudiantes uno al lado del otro. Las combinaciones son:

`[1, 3], [3, 4], [3, 5], [8, 10], [9, 10], [10, 12]`

Entonces, para el array del ejemplo, el programa debería devolver 6.
*/

const occupiedDesks = [12, 2, 6, 7, 11];

function options(arrayOfSeats) {
    const totalSeats = arrayOfSeats.shift();
    const occupiedSeats = arrayOfSeats;

    const seatOptions = [];

    for (let seat = 1; seat <= totalSeats; seat++) {
        if (seatIsNotOccupied(occupiedSeats, seat)) {
            const seatDown = Number(seat) + 2;
            if (seatDown <= totalSeats && seatIsNotOccupied(occupiedSeats, seatDown)) {
                seatOptions.push([seat, seatDown]);
            }

            if (seat % 2 !== 0) {
                const seatRight = Number(seat) + 1;
                if (seatIsNotOccupied(occupiedSeats, seatRight)) {
                    seatOptions.push([seat, seatRight]);
                }
            }
        }
    }
    return seatOptions;
}

// El "indexOf" haría lo mismo que el siguiente método (habría que utilizarlo en los "ifs" anteriores)
function seatIsNotOccupied(occupiedSeats, seat) {
    for (let occupiedSeat of occupiedSeats) {
        if (seat === occupiedSeat) {
            return false;
        }
    }
    return true;
}

console.log(options(occupiedDesks));